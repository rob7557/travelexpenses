//
//  UiViewControllerAddition.swift
//  TravelExpenses
//
//  Created by Robert Tratseuski on 7/19/19.
//  Copyright © 2019 Robert Tratseuski. All rights reserved.
//

import UIKit

extension UIViewController {
    static func getFromStoryboard(withId id: String) -> UIViewController? {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: id)
        return controller
    }
}
