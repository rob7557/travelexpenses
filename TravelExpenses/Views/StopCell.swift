//
//  StopCell.swift
//  TravelExpenses
//
//  Created by Robert Tratseuski on 7/19/19.
//  Copyright © 2019 Robert Tratseuski. All rights reserved.
//

import UIKit

class StopCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var transportImageView: UIImageView!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var moneyLabel: UILabel!
    @IBOutlet weak var rankingStarStackView: UIStackView!
    @IBOutlet weak var oneStar: UIImageView!
    @IBOutlet weak var twoStar: UIImageView!
    @IBOutlet weak var threeStar: UIImageView!
    @IBOutlet weak var fourStar: UIImageView!
    @IBOutlet weak var fiveStar: UIImageView!
    @IBOutlet weak var roundViewForStop: UIView!
    
}
