//
//  TravelCell.swift
//  TravelExpenses
//
//  Created by Robert Tratseuski on 7/19/19.
//  Copyright © 2019 Robert Tratseuski. All rights reserved.
//

import UIKit

class TravelCell: UITableViewCell {

    @IBOutlet weak var travelNameLabel: UILabel!
    @IBOutlet weak var travelSubtitleLabel: UILabel!
    
    @IBOutlet weak var rankTravel: UIStackView!
    
    @IBOutlet weak var roundView: UIView!
    @IBOutlet weak var oneStar: UIImageView!
    @IBOutlet weak var twoStar: UIImageView!
    @IBOutlet weak var threeStar: UIImageView!
    @IBOutlet weak var fourStar: UIImageView!
    @IBOutlet weak var fiveStar: UIImageView!

}
