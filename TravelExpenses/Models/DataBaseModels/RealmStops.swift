//
//  RealmStops.swift
//  TravelExpenses
//
//  Created by Robert Tratseuski on 7/19/19.
//  Copyright © 2019 Robert Tratseuski. All rights reserved.
//

import Foundation
import RealmSwift

class RealmStop: Object {
    
    @objc dynamic var id: String = UUID().uuidString
    @objc dynamic var money: String = ""
    @objc dynamic var name: String = ""
    @objc dynamic var rank: Int = 0
    @objc dynamic var transport: String = ""
    @objc dynamic var details: String = ""
    
    
    @objc dynamic var geo: RealmGeo?
    
    override static func primaryKey() -> String? {
        return "id"
    }
}

class RealmGeo: Object {
    
    @objc dynamic var id: String = UUID().uuidString
    
    @objc dynamic var lat: String = ""
    @objc dynamic var lng: String = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
