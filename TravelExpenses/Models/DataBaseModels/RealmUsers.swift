//
//  RealmUsers.swift
//  TravelExpenses
//
//  Created by Robert Tratseuski on 7/19/19.
//  Copyright © 2019 Robert Tratseuski. All rights reserved.
//

import RealmSwift
import UIKit

class RealmUsers: Object {
    @objc dynamic var name: String?
    @objc dynamic var email: String?
    @objc dynamic var id: String?
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    private func printAll() {
        print("all")
    }
}

extension RealmUsers {
    func sadasds() {
        printAll()
    }
}
