//
//  Stop.swift
//  TravelExpenses
//
//  Created by Robert Tratseuski on 7/19/19.
//  Copyright © 2019 Robert Tratseuski. All rights reserved.
//

import Foundation

class Stop {
    
    var money: String = ""
    var name: String = ""
    var rank: Int = 0
    var transport: String = ""
    var details: String = ""
    var rating: Int = 0
    func printAll() {
        print("""
            Name - \(name)
            Raiting - \(rank)
            Spent - \(money)
            Transpotr - \(transport)
            Description - \(details)
            """)
    }
}
