//
//  Travel.swift
//  TravelExpenses
//
//  Created by Robert Tratseuski on 7/19/19.
//  Copyright © 2019 Robert Tratseuski. All rights reserved.
//

import Foundation

class Travel {
    var name: String = ""
    var desc: String = ""
    var stops: [Stop] = []
    
}
