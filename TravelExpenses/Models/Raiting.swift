//
//  Raiting.swift
//  TravelExpenses
//
//  Created by Robert Tratseuski on 7/19/19.
//  Copyright © 2019 Robert Tratseuski. All rights reserved.
//

import Foundation

class Rank {
    var stopsRank: [Int] = []
    var rank: Int = 0
}
