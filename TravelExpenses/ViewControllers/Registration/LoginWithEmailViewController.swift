//
//  LoginWithEmailViewController.swift
//  TravelExpenses
//
//  Created by Robert Tratseuski on 7/17/19.
//  Copyright © 2019 Robert Tratseuski. All rights reserved.
//

import UIKit
import Firebase

class LoginWithEmailViewController: UIViewController {

    // MARK: - Outlets
    @IBOutlet weak var passwordView: UILabel!
    @IBOutlet weak var loginLabel: UITextField!
    @IBOutlet weak var viewMini: UIView!
    @IBOutlet weak var hideButton: UIButton!
    @IBOutlet weak var passwordLabelOutlet: UITextField!
    @IBOutlet weak var signInButtonOutlet: UIButton!
    
    // MARK: - Actions
    
    @IBAction func forgotDidTapped(_ sender: Any) {
        let controller = UIViewController.getFromStoryboard(withId: "forgotVC")!
        navigationController?.pushViewController(controller, animated: true)
    }
    @IBAction func loginButtonAction(_ sender: Any) {
        
        if let login = loginLabel.text, let password = passwordLabelOutlet.text {
            Auth.auth().signIn(withEmail: login, password: password) { (result, error) in
                if error == nil {
                    let user = result?.user
                    print(user?.email)
                    
                    let travelVC = UIViewController.getFromStoryboard(withId: "TravelListVC")
                    self.navigationController?.pushViewController(travelVC!, animated: true)
                } else {
                    print("Password Error!")
                    self.viewMini.backgroundColor = .red
                    self.passwordView.textColor = .red
                }
            }
            
        }
    }
    
    @IBAction func back(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func hideButtonAction(_ sender: Any) {
        if passwordLabelOutlet.isSecureTextEntry == true {
            passwordLabelOutlet.isSecureTextEntry = false
            hideButton.setImage(UIImage(named: "view"), for: .normal)
        } else {
            passwordLabelOutlet.isSecureTextEntry = true
            hideButton.setImage(UIImage(named: "hide"), for: .normal)
        }
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        signInButtonOutlet.layer.cornerRadius = 20
        signInButtonOutlet.layer.masksToBounds = true
    }

}
