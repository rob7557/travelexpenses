//
//  LoginViewController.swift
//  TravelExpenses
//
//  Created by Robert Tratseuski on 7/16/19.
//  Copyright © 2019 Robert Tratseuski. All rights reserved.
//

import UIKit
import Firebase

class WelcomeViewController: UIViewController {
    // MARK: - Outlets
    
    @IBOutlet weak var moreWaysToLoginButton: UIButton!
    @IBOutlet weak var createAnAccountButton: UIButton!
    @IBOutlet weak var loginWithEmailButton: UIButton!
    
    // MARK: - Actions
    
    @IBAction func reservButtonDidTapped(_ sender: Any) {
        let controller = UIViewController.getFromStoryboard(withId: "TravelListVC")!
        navigationController?.pushViewController(controller, animated: true)
    }
    @IBAction func loginWithEmailTapped(_ sender: Any) {
        let loginController = UIViewController.getFromStoryboard(withId: "Login")!
        navigationController?.pushViewController(loginController, animated: true)
    }
    
    @IBAction func registrationTapped(_ sender: Any) {
        // new accaunt
        let loginController = UIViewController.getFromStoryboard(withId: "CreateAnAccountVC")
        navigationController?.pushViewController(loginController!, animated: true)
    }
      // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = true
        createAnAccountButton.layer.cornerRadius = 20
        createAnAccountButton.layer.masksToBounds = true
        
        loginWithEmailButton.layer.cornerRadius = 20
        loginWithEmailButton.layer.masksToBounds = true
    }

}
