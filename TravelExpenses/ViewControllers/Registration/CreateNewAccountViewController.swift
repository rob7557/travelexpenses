//
//  CreateNewAccountViewController.swift
//  TravelExpenses
//
//  Created by Robert Tratseuski on 7/17/19.
//  Copyright © 2019 Robert Tratseuski. All rights reserved.
//

import UIKit
import Firebase

class CreateNewAccountViewController: UIViewController {

    // MARK: - Properties
    
    // MARK: - Outlets
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var hideButtonOutlet: UIButton!
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var signUpButtonOutlet: UIButton!
    // MARK: - Actions
    
    @IBAction func signUpButton(_ sender: Any) {
        if let login = loginTextField.text, let password = passwordTextField.text {
            Auth.auth().createUser(withEmail: login, password: password) { (result, error) in
                if error == nil {
                    
                    self.errorLabel.textColor = .green
                    self.passwordLabel.textColor = .green
                    self.emailLabel.textColor = .green
                    self.errorLabel.text = "successful"
                    
                    self.navigationController?.popViewController(animated: true)
                    
                } else {
                    
                    switch error.unsafelyUnwrapped.localizedDescription {
                        
                    case "The email address is already in use by another account.":
                        self.emailLabel.textColor = .red
                        self.emailLabel.text = "The email address is already in use by another account."
                        
                    case "The password must be 6 characters long or more.":
                        self.passwordLabel.textColor = .red
                        self.passwordLabel.text = "The password must be 6 characters long or more"
                        
                    case "An email address must be provided.":
                        self.emailLabel.textColor = .red
                        self.emailLabel.text = "Uncorrect type of email address"
                        
                    case "The email address is badly formatted.":
                        self.emailLabel.textColor = .red
                        self.emailLabel.text = "Uncorrect type of email address"
                        
                    default:
                        print(error.unsafelyUnwrapped.localizedDescription)
                        self.errorLabel.textColor = .red
                        self.errorLabel.text = "error"
                    }
                }
            }
        }
    }
    
    
    @IBAction func hideButtonAction(_ sender: Any) {
        if passwordTextField.isSecureTextEntry == true {
            passwordTextField.isSecureTextEntry = false
            hideButtonOutlet.setImage(UIImage(named: "view"), for: .normal)
        } else {
            passwordTextField.isSecureTextEntry = true
            hideButtonOutlet.setImage(UIImage(named: "hide"), for: .normal)
        }
    }
    
    @IBAction func backDidTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        signUpButtonOutlet.layer.cornerRadius = 20
        signUpButtonOutlet.layer.masksToBounds = true
    }

}
