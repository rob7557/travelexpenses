//
//  ForgotPasswordViewController.swift
//  TravelExpenses
//
//  Created by Robert Tratseuski on 7/17/19.
//  Copyright © 2019 Robert Tratseuski. All rights reserved.
//

import UIKit
import Firebase

class ForgotPasswordViewController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var signUpButtonOutlet: UIButton!
    
    //MARK: - Actions
    @IBAction func back(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func forgotPasswordDidTapped(_ sender: Any) {
        if let email = emailTextField.text {
            Auth.auth().sendPasswordReset(withEmail: email) { (error) in
                if error == nil {
                    print("New password sent succesful!")
                } else {
                    print("New password unsent!")
                }
            }
        }
    }
    //MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        signUpButtonOutlet.layer.cornerRadius = 20
        signUpButtonOutlet.layer.masksToBounds = true
    }
}
