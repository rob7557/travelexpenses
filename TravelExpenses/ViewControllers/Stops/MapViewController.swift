//
//  MapViewController.swift
//  TravelExpenses
//
//  Created by Robert Tratseuski on 7/19/19.
//  Copyright © 2019 Robert Tratseuski. All rights reserved.
//

import UIKit
import MapKit

protocol MapViewControllerDelegate {
    func mapControllerDidSelectedPoint(_ point: MKPointAnnotation)
}

class MapViewController: UIViewController {
    
    // MARK: - Properties
    var array: [MKPointAnnotation]?
    var delegate: MapViewControllerDelegate?
    
    // MARK: - Outlets
    @IBOutlet weak var mapView: MKMapView!
    
    
    // MARK: - Actions
    @IBAction func pointDidChoose(_ recognizer: UITapGestureRecognizer) {
        let point = recognizer.location(in: mapView)
        let tapPoint = mapView.convert(point, toCoordinateFrom: mapView)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = tapPoint
        
        mapView.removeAnnotations(mapView.annotations)
        mapView.addAnnotation(annotation)
    }
    
    
    @IBAction func saveDidTapped(_ sender: Any) {
        if let selectedPoint = mapView.annotations.first as? MKPointAnnotation {
            delegate?.mapControllerDidSelectedPoint(selectedPoint)
        }
        navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let array = array {
            for point in array {
                mapView.addAnnotation(point)
            }
        }
    }
    
}

