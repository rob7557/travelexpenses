//
//  CreateStopViewController.swift
//  TravelExpenses
//
//  Created by Robert Tratseuski on 7/18/19.
//  Copyright © 2019 Robert Tratseuski. All rights reserved.
//

import RealmSwift
import UIKit
import MapKit

protocol CreteStopViewControllerDelegate {
    func createStopControllerDidCreateStop(_ stop: RealmStop)
}

class CreateStopViewController: UIViewController {
    
    // MARK: - Properties
    var stop = RealmStop()
    var delegate: CreteStopViewControllerDelegate?
    var delegateFromStopListViewController: StopListViewController?
    var mapDelegate: MapViewController?
    var delegateStop: StopListViewController?
    
    
    var stopDidCreateClosure: ((RealmStop) -> Void)?
    
    // MARK: - Outlets
    @IBOutlet weak var locationInfoTextField: UILabel!
    @IBOutlet weak var locationInfoLng: UILabel!
    @IBOutlet weak var rankStepper: UIStepper!
    
    @IBOutlet weak var labelForError: UILabel!
    @IBOutlet weak var moneySpentLabel: UILabel!
    @IBOutlet weak var nameTextField: UITextField! //label
    @IBOutlet weak var rankingLabel: UILabel!
    @IBOutlet weak var transportSegmentControl: UISegmentedControl!
    @IBOutlet weak var details: UITextView!
    
    
    // MARK: - Actions
    @IBAction func saveButtonTapped(_ sender: Any) {
        if ((nameTextField.text != "") && (rankingLabel.text != "") && (details.text != "")) {
            labelForError.textColor = .black
        let realm = try! Realm()
        try! realm.write {
            let geo = RealmGeo()
            geo.lat = String(locationInfoTextField.text!)
            geo.lng = String(locationInfoLng.text!)
            stop.geo = geo
            
            if let text = nameTextField.text {
                stop.name = text
            }
            if let text = moneySpentLabel.text {
                stop.money = text
            }
            if let rank = rankingLabel.text {
                delegateStop?.arrayStopsRank.append(Int(rank)!)
                stop.rank = Int(rank)!
            }
            if let text = details.text {
                stop.details = text
            }
            
            switch transportSegmentControl.selectedSegmentIndex {
            case 0:
                stop.transport = "Jet"
            case 1:
                stop.transport = "Train"
            case 2:
                stop.transport = "Car"
            default:
                stop.transport = ""
                print("transportSegmentControl ERROR - nil")
            }
            realm.add(stop, update: true)
            delegateFromStopListViewController?.tableView.reloadData()
        }
        
        //stop.geo?.lat = locationInfoTextFieldlat
        
        //delegateFromStopListViewController?.tableView.reloadData()
        
        stopDidCreateClosure?(stop)
        //delegate?.createStopControllerDidCreateStop(stop)
        navigationController?.popViewController(animated: true)
        } else {
            labelForError.text = "Please fill out every single line!"
            labelForError.textColor = .red
        }
    }
    
    @IBAction func addMap(_ sender: Any) {
//        let minsk = MKPointAnnotation()
//        minsk.coordinate = CLLocationCoordinate2D(latitude: 53.925, longitude: 27.508)
//
//        let moscow = MKPointAnnotation()
//        moscow.coordinate = CLLocationCoordinate2D(latitude: 55.668, longitude: 37.689)
        
        let mapVC = UIViewController.getFromStoryboard(withId: "MapVC") as! MapViewController
//        mapVC.array = [minsk, moscow]
        mapVC.delegate = self
        navigationController?.pushViewController(mapVC, animated: true)
    }
    
    @IBAction func chooseCurency(_ sender: Any) {
        let spendMoneyVc = UIViewController.getFromStoryboard(withId: "SpentMoney")
        navigationController?.pushViewController(spendMoneyVc!, animated: true)
        if let controller = spendMoneyVc as? SpentMoneyViewController {
            controller.delegate = self
        }
    }
    
    @IBAction func chooseRankDidTapped(_ sender: UIStepper) {
        rankingLabel.text = Int(sender.value).description
    }
    
    @IBAction func cancelDidTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    func userSpentMoney(_ moneyCount: String) {
        print("I spent \(moneyCount)")
        moneySpentLabel.text = moneyCount
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        nameTextField.text = stop.name
        rankingLabel.text = String(stop.rank)
        rankStepper.value = Double(stop.rank)
        locationInfoTextField.text = stop.geo?.lat
        locationInfoLng.text = stop.geo?.lng
        moneySpentLabel.text = stop.money
        if stop.transport == "Jet" {
            transportSegmentControl.selectedSegmentIndex = 0
        } else if stop.transport == "Train" {
            transportSegmentControl.selectedSegmentIndex = 1
        } else if stop.transport == "Car" {
            transportSegmentControl.selectedSegmentIndex = 2
        }
        details.text = stop.details
    }
    
    
    
}


extension CreateStopViewController: MapViewControllerDelegate {
    func mapControllerDidSelectedPoint(_ point: MKPointAnnotation) {
        print("MapView work")
        locationInfoLng.text = String(point.coordinate.longitude)
        locationInfoTextField.text = String(point.coordinate.latitude)
    }
}
