//
//  SpentMoneyViewController.swift
//  TravelExpenses
//
//  Created by Robert Tratseuski on 7/19/19.
//  Copyright © 2019 Robert Tratseuski. All rights reserved.
//

import UIKit

class SpentMoneyViewController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var segmentCurrency: UISegmentedControl!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var ConfirmButton: UIButton!
    
    // MARK: - Properties
    var delegate: CreateStopViewController?
    
    // MARK: - Actions
    @IBAction func readyClicked(_ sender: Any) {
        if let text = textField.text {
            if text != "" {
                if let intString = Int(text) {
                    switch segmentCurrency.selectedSegmentIndex {
                    case 0:
                        delegate?.userSpentMoney("\(text)€")
                    case 1:
                        delegate?.userSpentMoney("\(text)$")
                    case 2:
                        delegate?.userSpentMoney("\(text)₽")
                    default:
                        print("error segmentControl")
                    }
                    navigationController?.popViewController(animated: true)
                } else {
                    label.textColor = .red
                    label.text = "Input number"
                }
            } else {
                label.textColor = .red
                label.text = "Input spent amount"
                
            }
            
        }
    }
    
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        textField.keyboardType = .numberPad // UIKeyboardType.numberPad
        self.hideKeyboard()
        ConfirmButton.layer.cornerRadius = 20
        ConfirmButton.layer.masksToBounds = true
    }
}




extension UIViewController {
    func hideKeyboard()
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(UIViewController.dismissKeyboard))
        
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard()
    {
        view.endEditing(true)
    }
}

