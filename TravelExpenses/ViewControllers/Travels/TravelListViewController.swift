//
//  TravelListViewController.swift
//  TravelExpenses
//
//  Created by Robert Tratseuski on 7/18/19.
//  Copyright © 2019 Robert Tratseuski. All rights reserved.
//

import UIKit

class TravelListViewController: UIViewController {
    
    // MARK: - Properties
    var delegateStopList: StopListViewController?
    var travels: [RealmTravel] = []
    var travelName = ""
    var dataBaseManager : DataBaseManager?
    
    // MARK: - Outlets
    @IBOutlet weak var noTravellabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Actions
    @IBAction func addTravelDidTapped(_ sender: Any) {
        let alertController = UIAlertController(title: "Enter country name:", message: nil, preferredStyle: .alert)
        let createAction = UIAlertAction(title: "Create", style: .default) { (_) in
            let countryTextField = alertController.textFields![0] as UITextField
            let descriptionTextField = alertController.textFields![1] as UITextField
            
            let travel = RealmTravel()
            if let country = countryTextField.text {
                travel.name = country
            }
            if let description = descriptionTextField.text {
                travel.desc = description
            }
            
            self.dataBaseManager?.saveTravelToDataBase(travel: travel)
            self.noTravellabel.isHidden = true
            self.travels.append(travel)
            self.tableView.reloadData()
        }
        createAction.isEnabled = true
        alertController.addAction(createAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in
        }
        alertController.addAction(cancelAction)
        
        // ADD TEXT FIELDS
        alertController.addTextField { (textField) in
            textField.placeholder = "Country"
        }
        alertController.addTextField { (textField) in
            textField.placeholder = "Description"
            
            NotificationCenter.default.addObserver(forName: UITextField.textDidChangeNotification, object: textField, queue: OperationQueue.main) { (notification) in
                createAction.isEnabled = textField.text != ""
            }
        }
        // PRESENT
        present(alertController, animated: true)
    }
    
    // MARK: - Lifecycle
    override func viewWillAppear(_ animated: Bool) {
        if travels.count != 0 {
            noTravellabel.isHidden = true
        } else {
            noTravellabel.isHidden = false
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        travels = DataBaseManager.instance.getObjects(RealmTravel.self)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        
    }
}

extension TravelListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return travels.count
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let travel = travels[indexPath.row]
            travels.remove(at: indexPath.row)
            DataBaseManager.instance.deleteTravelFromDataBase(travel)
            self.tableView.deleteRows(at:[indexPath],with: .fade)
            self.tableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "travelCell", for: indexPath) as! TravelCell
        
        let travel = travels[indexPath.row]
        travelName = travel.name
        cell.travelNameLabel.text = travel.name
        cell.travelSubtitleLabel.text = travel.desc
        
        cell.roundView.layer.cornerRadius = 20
        cell.roundView.layer.masksToBounds = true
        
        let average = travel.getAverageRating()
        
        switch average {
            
        case 0:
            cell.oneStar.image = UIImage(named: "StarOff")
            cell.twoStar.image = UIImage(named: "StarOff")
            cell.threeStar.image = UIImage(named: "StarOff")
            cell.fourStar.image = UIImage(named: "StarOff")
            cell.fiveStar.image = UIImage(named: "StarOff")
            print("0")
            
        case 1:
            cell.oneStar.image = UIImage(named: "StarOn")
            cell.twoStar.image = UIImage(named: "StarOff")
            cell.threeStar.image = UIImage(named: "StarOff")
            cell.fourStar.image = UIImage(named: "StarOff")
            cell.fiveStar.image = UIImage(named: "StarOff")
            print("1")
            
        case 2:
            cell.oneStar.image = UIImage(named: "StarOn")
            cell.twoStar.image = UIImage(named: "StarOn")
            cell.threeStar.image = UIImage(named: "StarOff")
            cell.fourStar.image = UIImage(named: "StarOff")
            cell.fiveStar.image = UIImage(named: "StarOff")
            print("2")
            
        case 3:
            cell.oneStar.image = UIImage(named: "StarOn")
            cell.twoStar.image = UIImage(named: "StarOn")
            cell.threeStar.image = UIImage(named: "StarOn")
            cell.fourStar.image = UIImage(named: "StarOff")
            cell.fiveStar.image = UIImage(named: "StarOff")
            print("3")
            
        case 4:
            cell.oneStar.image = UIImage(named: "StarOn")
            cell.twoStar.image = UIImage(named: "StarOn")
            cell.threeStar.image = UIImage(named: "StarOn")
            cell.fourStar.image = UIImage(named: "StarOn")
            cell.fiveStar.image = UIImage(named: "StarOff")
            print("4")
            
        case 5:
            cell.oneStar.image = UIImage(named: "StarOn")
            cell.twoStar.image = UIImage(named: "StarOn")
            cell.threeStar.image = UIImage(named: "StarOn")
            cell.fourStar.image = UIImage(named: "StarOn")
            cell.fiveStar.image = UIImage(named: "StarOn")
            print("5")
            
        default:
            print("errorInAverageRank(TravelsListViewController)")
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let stopVC = UIViewController.getFromStoryboard(withId: "StopVC") as! StopListViewController
        stopVC.realmTravel = travels[indexPath.row]
        stopVC.delegate = self
        navigationController?.pushViewController(stopVC, animated: true)
    }
}
